
public class App {
	
	static double area;
	
	public static double calcularArea(double raio) 
	{
		return 3.14 * Math.pow(raio, 2);
	}
	
	public static void calcularArea(float raio) {
		area = 3.14 * Math.pow(raio, 2);
	}

	public static void main(String[] args) {
		
		calcularArea(2.55);

	}

}
