
public class appBanco {

	public static void main(String[] args) {
		 
		Conta C1 = new Conta(1255456, 23000);
		C1.sacar(200);
		
		Conta C2 = new Conta(554544,2300, true);
		C2.sacar(300);
		
		if(C2.isAtiva()) {
			System.out.println(C2.getNumero());	
			System.out.println(C2.getSaldo());
			System.out.println("----------------");
		}

		if(C1.isAtiva()) {
			System.out.println(C1.getNumero());	
			System.out.println(C1.getSaldo());
			System.out.println("----------------");
		}
		
	}

}
