import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Exercicio6 {
	
	static Scanner tecla = new Scanner(System.in);
	static Calendar cal = GregorianCalendar.getInstance();

	public static void main(String[] args) 
	{
		int anoAtual = Calendar.getInstance().get(Calendar.YEAR);		
		int ano = 0;
		
		System.out.println("Ano de nascimento c/ quatro digitos");
		ano = tecla.nextInt();
		
		if(( anoAtual - ano ) >= 16)
		{
			System.out.println("Voce podera votar este ano! :(");
		}
		else 
		{
			System.out.println("Voce ainda nao podera votar este ano! :)");
		}

	}

}
