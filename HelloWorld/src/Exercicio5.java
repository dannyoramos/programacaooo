import java.util.Scanner;

public class Exercicio5 {
	
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		
		double valor = 0;
		
		System.out.println("Digite um valor!");
		valor = tecla.nextDouble();
		
		if(valor < 0) {
			System.out.println("Negativo!");
		}
		else {
			System.out.println("Positivo!");
		}

	}

}
