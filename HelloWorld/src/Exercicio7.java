import java.util.Scanner;

public class Exercicio7 {
	
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		
		int senha = 0;
		
		System.out.println("Digite a senha. (quatro digitos)");
		senha = tecla.nextInt();
		
		if(senha == 1234) 
		{
			System.out.println("Acesso permitido!");
		}
		else 
		{
			System.out.println("Acesso negado!");
		}

	}

}
