
import java.util.Scanner;

public class Exercicio1 {
	

	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
			
		double raio, area;
		final double PI = 3.14;
		
		System.out.println("Digite o valor do raio: ");
		raio = tecla.nextDouble();
		
		area = PI * Math.pow(raio,2);
		
		
		System.out.println("Area do circulo: " + area);
		
	}
}