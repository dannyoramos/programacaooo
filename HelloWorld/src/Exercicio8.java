import java.util.Scanner;

public class Exercicio8 {
	
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		
		int num = 0;
		
		System.out.println("Digite um numero:");
		num = tecla.nextInt();
		
		if(num % 2 == 0) 
		{
			System.out.println("O numero digitado � Par!");
		}
		else
		{
			System.out.println("O numero digitado � impar!");
		}

	}

}
