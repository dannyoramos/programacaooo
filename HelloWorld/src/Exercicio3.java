import java.util.Scanner;

public class Exercicio3 {

	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		double comp = 0, larg = 0, alt = 0, area = 0;
		double caixas = 0;
		
		System.out.println("Qual o comprimento da cozinha?");
		comp = tecla.nextDouble();
		
		System.out.println("Qual a largura da cozinha?");
		larg = tecla.nextDouble();
		
		System.out.println("Qual a altura da cozinha?");
		alt = tecla.nextDouble();
		
		area = (comp * alt * 2) + (larg * alt * 2);
		
		caixas = Math.round(area/1.5);
		
		System.out.println("Quantidade de caixas de azuleijos para colocar em todas as paredes:" + caixas);

	}

}
