import java.util.Scanner;

public class Exercicio2 {
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		double temp_f = 0;
		double temp_c = 0;
		
		System.out.println("Informe a temperatura em graus Fahrenheit:");
		temp_f = tecla.nextDouble();
		
		temp_c = ((temp_f - 32) * 5) / 9;
		
		System.out.println("A temperatura em graus Celsius �:" + temp_c);
	}

}
