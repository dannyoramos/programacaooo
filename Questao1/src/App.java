import java.util.Scanner;

public class App {
	
	static Scanner tecla = new Scanner(System.in);
	static Pessoa[] lista = new Pessoa[20];
	
	static int index = 0;
	
	public static void main(String[] args) {
		
		int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Inserir Pessoa");
            System.out.println("2-Listar Pessoas");
            System.out.println("3-Sair");
            System.out.println("Digite sua opcao: ");
            op = tecla.nextInt(); 
            switch(op){
            case 1: inserirPessoa(); break;
            case 2: listarPessoa(); break;
            case 3: break;
            }
        } while (op!=3); 
        		
	}
	
	public static void inserirPessoa() {
    	
    	System.out.println("Digite o Nome");
    	String nome = tecla.next();
    	System.out.println("Digite o CPF");
    	int cpf = tecla.nextInt();
    	System.out.println("Digite a Data de Nascimento");
    	String nascimento = tecla.next();
    	System.out.println("Digite o Endere�o");
    	String endereco = tecla.next();
    	
    	lista[index++] = new Pessoa(nome, cpf, nascimento, endereco);
    	System.out.println("Cadastro realizado com sucesso!");
    }
	
	public static void listarPessoa() {
		System.out.println("Digite o N� do CPF");
		for(int i = 0; i < lista.length-1; i++) {
			if(lista[i] != null) {
				System.out.println(lista[i].getNome() 
						+ "\n --------------------" +
						lista[i].getCpf() 
						+ "\n -------------------" +
						lista[i].getNascimento()
						+ "\n -------------------" +
						lista[i].getEndereco());
			}else {
				break;
			}
		}
	}

}
